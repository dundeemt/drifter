'''tests for the Drift class'''
import sys
# from click.testing import CliRunner
sys.path.append('..')
from drifter import Drift   # NOQA


def test_repr():
    drift = Drift(home='Drifterfile', drifter_vms={}, available_vms={})
    assert str(drift) == "<Drift 'Drifterfile'>"


def test_not_found_vm(capsys):
    vms = [{'name': 'NOTFOUND',
            'uuid': '00000000-0000-0000-0000-000000000000'}]
    drift = Drift(home='Drifterfile', drifter_vms=vms, available_vms={})  # NOQA
    out, err = capsys.readouterr()
    assert out == 'Error: <vm "NOTFOUND" - 00000000-0000-0000-0000-000000000000> : UUID not found in Available VMs\n'  # NOQA


def test_remove_avms_notfound():
    vms = [{'name': 'NOTFOUND',
            'uuid': '00000000-0000-0000-0000-000000000000'},
           {'name': 'FOUND',
            'uuid': '00000000-0000-0000-0000-000000000001'}]
    drift = Drift(home='Drifterfile', drifter_vms=vms, available_vms={})  # NOQA
    print "dvms:", drift.drifter_raw
    drift.remove('NOTFOUND')
    assert drift.drifter_raw == [{'name': 'FOUND',
                                  'uuid': '00000000-0000-0000-0000-000000000001'}]
