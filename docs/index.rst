.. drift documentation master file, created by
   sphinx-quickstart on Sun May 11 08:53:24 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to drifter's documentation!
==================================
drifter is a simplified control for virtualboxes that doesn't require the vm to be packaged in a specific and ill documented format.  Which allows use to create your environment with Virtualbox and then drift will take care of bringing them up and shutting them down.

Example
-------
.. code-block:: bash

    $ drifter up

Supports
--------
Tested on Python 2.7


Additional Information
----------------------

* Project:  https://bitbucket.org/dundeemt/drifter
* Download: https://pypi.python.org/pypi/drifter
* Documentation: http://drifter.rtfd.org/
* License: BSD


Contents:

.. toctree::
   :maxdepth: 2

   changes


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

