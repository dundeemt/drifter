Changes
=======

v0.0.3
------

 - bug fixes for removing a non-existent VM from Drifterfile
 - tests

v0.0.2
------

 - updated documentation links

v0.0.1
------

- Initial Release
